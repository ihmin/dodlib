#ifndef __IMS_H_
#define __IMS_H_

#include <string>
#include <memory.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "bnk.h"

#define MAX_VOICE 11

#ifdef __cplusplus
extern "C" {
#endif

#pragma pack(push, 1)
#pragma pack(1)
typedef struct {
	unsigned char nVerMajor;		// 1 byte
	unsigned char nVerMinor;		// 1 byte
	long int nTuneID;				// 4 byte
	char szTuneName[30];			// 30 byte
	unsigned char nTickBeat;		// 1 byte
	unsigned char nBeatMeasure;		// 1 byte
	long int nTotalTick;			// 4 byte
	long int cbDataSize;			// 4 byte
	long int nrCommand;				// 4 byte
	unsigned char nSrcTickBeat;		// 1 byte
	char filler[7];					// 7 byte
	unsigned char nSoundMode;		// 1 byte
	unsigned char nPitchBRange;		// 1 byte
	unsigned short int nBasicTempo;	// 2 byte
	char filler2[8];				// 8 byte
} IMS_HEADER;						// 70 byte
#pragma pack(pop)

typedef struct {
	char name[8+1];
    int	index;						// BNK 파일에서의 인덱스 번호
} IMS_INST;

typedef struct {
	unsigned char *raw;				// IMS 파일 내용 전체를 통째로 읽어올 버퍼
	unsigned char *rawPtr;
//	----------------------------------------------------------------------------
	IMS_HEADER header;
	unsigned char *songData;
//	----------------------------------------------------------------------------
	int instCount;					// 사용된 악기수
	IMS_INST *instIndex; 
//	----------------------------------------------------------------------------
	BNK	*m_bnk;
} IMS;

#ifdef __cplusplus
}
#endif

class Ims
{
public:
	IMS *m_ims;
	
	int	m_fileSize;			// raw 버퍼로 읽어들인 실제 크기

	int m_basicTempo;
	int	m_songDataIndex;
	int	m_songDataTotalIndex;
	int m_tick;

	int m_channelNote[MAX_VOICE];
	int m_channelVolume[MAX_VOICE];
	int m_channelPitch[MAX_VOICE];
	std::string m_channelInstrument[MAX_VOICE];

public:
	Ims();
	~Ims();

	IMS *GetHeader();
	long GetTotalTick();
	long GetDuration();
	char* GetTitle();
	int GetBasicTempo();
	int GetCurrentTick();
	bool Open(char* file);
	void Rewind(void);
	int Play(void);
	bool MatchBnk(BNK *bnk);
	bool Reset(void);
	int GetChannelNote(int channel);
	int GetChannelVolume(int channel);
	int GetChannelPitch(int channel);
	std::string GetChannelInstrument(int channel);
};

#endif	//__IMS_H_
