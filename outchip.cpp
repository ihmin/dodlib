/*
	Copyright Ad Lib Inc., 1990

	This file is part of the Ad Lib Programmer's Manual product and is
	subject to copyright laws.  As such, you may make copies of this file
	only for the purpose of having backup copies.  This file may not be
	redistributed in any form whatsoever.

	If you find yourself in possession of this file without having received
	it directly from Ad Lib Inc., then you are in violation of copyright
	laws, which is a form of theft.


	OUTCHIP.ASM

	Adlib Inc, 20-avr-89
*/


#include "outchip.h"
#include "opl2board.h"

// Set the register at address 'addr' to the value 'data'.
// Take care of data register to data register write delay.
void SndOutput(OPL2AudioBoard *opl, int addr, int dataVal)
{
	opl->write(addr, dataVal);
	//YM3812Write(ym3812p, 0, addr);
	//YM3812Write(ym3812p, 1, dataVal);
}
