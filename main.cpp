#include "opl2board.h"
#include "adlib.h"
#include "outchip.h"
#include "ims.h"
#include "bnk.h"
#include "convert.h"
#include "main.h"
#include <stdlib.h>
#include <time.h>
#include <windows.h>
#include <signal.h>
#include <sys/stat.h>
#include <unistd.h>
#include <chrono>
#include <string>
#include <list>
#include <iostream>
#include <fstream>

#define REPEAT_NO		0	// 지금 곡 연주하고 중지
#define REPEAT_ALL		1	// 지금 곡 연주 끝나면 다음곡으로..
#define REPEAT_THIS		2	// 지금 곡 반복 연주

const char *PIT_METER[]={
	"C ", "C#", "D ", "D#", "E ", "F ", "F#", "G ", "G#", "A ", "A#", "B "
};

				
int m_playMode = STOPPED;
int m_repeatMode = REPEAT_THIS;

OPL2AudioBoard *opl;


char GetKey(void)
{
	fflush(stdin);
	char c;
	c = std::cin.get();
	return c;
}

bool ExistFile (const std::string& name) {
    return ( access( name.c_str(), F_OK ) != -1 );
}

void SplitFilename (const std::string& str, std::string &folder, std::string &filename)
{
	size_t found;
	found=str.find_last_of("/\\");
	folder = str.substr(0,found);
	filename = str.substr(found+1);
}

bool GetAvailablePorts()
{
    char lpTargetPath[5000]; // buffer to store the path of the COMPORTS
	bool got = false;

    for (int i = 0; i < 255; i++) // checking ports from COM0 to COM255
    {
        std::string str = "COM" + std::to_string(i); // converting to COM0, COM1, COM2
        DWORD test = QueryDosDevice(str.c_str(), lpTargetPath, 5000);

        // Test the return value and error if any
        if (test != 0) //QueryDosDevice returns zero if it didn't find an object
        {
            std::cout << "  " + str << ": " << lpTargetPath << std::endl;
			got = true;
        }

        if (::GetLastError() == ERROR_INSUFFICIENT_BUFFER)
        {
        }
    }

	return got;
}

void CleanUp(int s)
{
	SoundWarmInit();

	for (int i=0; i<MAX_VOICE; i++) {
		NoteOff(i);
		SetVoiceVolume(i, 0);
	}

	opl->reset();

	exit(0);
}

void GotoXY(int x, int y)
{
	COORD pos={(SHORT)x, (SHORT)y};
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), pos);
}

void ShowCursor(bool showFlag)
{
    HANDLE out = GetStdHandle(STD_OUTPUT_HANDLE);

    CONSOLE_CURSOR_INFO     cursorInfo;

    GetConsoleCursorInfo(out, &cursorInfo);
    cursorInfo.bVisible = showFlag; // set the cursor visibility
    SetConsoleCursorInfo(out, &cursorInfo);
}

unsigned long _stdcall Update(void*p)
{
	Ims *ims = (Ims*)p;
	
	char p_note[MAX_VOICE];
	for(int i=0; i<MAX_VOICE; i++) {
		p_note[i]=0;
	}
 
    while(1)
    {
		//WaitForSingleObject(hTimer, 5000);
		
		GotoXY(0, 10);
		printf("Current Tick: %6d/%6d\r", ims->GetCurrentTick(), ims->GetTotalTick());

		GotoXY(0, 12);
		printf("Channel");
		GotoXY(10, 12);
		printf("Note");
		GotoXY(20, 12);
		printf("Volume");
		GotoXY(30, 12);
		printf("Pitch");
		GotoXY(40, 12);
		printf("Instrument");

		for(int ch=0; ch<MAX_VOICE; ch++) {
			GotoXY(0, 13+ch);
			printf("CH#%02d", ch+1);

			GotoXY(10, 13+ch);
			char note = ims->GetChannelNote(ch);
			if(note<12) note=12;
			if(note>=128) note=127;
			if(p_note[ch]!=note) {
				p_note[ch]=note;
				printf("%s%d", PIT_METER[note%12], (int)(note/12)-1);
			}

			GotoXY(20, 13+ch);
			printf("%-10d", ims->GetChannelVolume(ch));
			
			GotoXY(30, 13+ch);
			int p=((ims->GetChannelPitch(ch)-(int)0x2000) >> 6);
			if (p) {
				if(p>0) 
					printf("+%-10d", p);
				else
					printf("-%-10d", -p);
			}

			GotoXY(40, 13+ch);
			std::string inst = ims->GetChannelInstrument(ch);
			printf("%-20s", ims->GetChannelInstrument(ch).c_str());
		}

    }
    return 0;
}

int main(int argc, char **argv)
{
	ShowCursor(false);

	signal(SIGINT, CleanUp);
	signal(SIGBREAK, CleanUp);

	int portnum;
	if ( getenv("DODLIB_COMPORT") == NULL ) {
		std::cout << "Installed COM Ports:" << std::endl;

		bool got = GetAvailablePorts();
		if (got == false) {
			std::cout << "  Not found in your system.\n";
			GetKey();
			exit(1);
		}

		std::cout << "\n";
		std::cout << "Please type the number of COM port: ";

		char c = GetKey();
		char tmp[100];
		sprintf(tmp, "%c", c);
		portnum = atoi(tmp);

	} else {
		portnum = atoi(getenv("DODLIB_COMPORT"));
	}
	
	// -----------------------------------------------------------
	std::cout << "\n";

	std::string folder;
	std::string filename;

	// 먼저 ims 파일 위치에 bnk 파일이 있는지 검사..
	SplitFilename (argv[1], folder, filename);
	std::string bnk_file = folder + "\\" + "STANDARD.BNK";
	if ( !ExistFile(bnk_file) ) {
		// 없으면 ims.exe 폴더내에 있는지 검사
		SplitFilename (argv[0], folder, filename);
		bnk_file = folder + "\\" + "STANDARD.BNK";

		if ( !ExistFile(bnk_file) ) {
			std::cout << "STANDARD.BNK file not found." << std::endl;
			GetKey();
			exit(1);
		}
	}

	opl = new OPL2AudioBoard;
	char portname [12];
	sprintf(portname, "COM%d", portnum);
	if ( !opl->connect(portname) ) {
		GetKey();
		exit(1);
	}
	opl->reset();

	Bnk *bnk = new Bnk();
	bnk->Open((char*)bnk_file.c_str());

	Ims *ims = new Ims();
	ims->Open(argv[1]);
	ims->MatchBnk(bnk->GetBnk());

	printf("BNK: %s\n", bnk_file.c_str());
	printf("IMS: %s\n", argv[1]);

	char buf[255];
	han_conv(0, ims->GetTitle(), buf);
	printf("Title: %s\n", buf);
	fflush(stdout);

	SoundWarmInit();

	SndOutput(opl, 1, 0x20);	// Enable waveform select (bit 5)
	IMS *header = ims->GetHeader();
	SetMode(header->header.nSoundMode);

	for (int i=0; i<MAX_VOICE; i++) {
		NoteOff(i);
		SetVoiceVolume(i, 0);
	}

	long sec = ims->GetDuration();
	int h = sec/3600;
	int m = (sec%3600)/60;
	int s = (sec%3600)%60;
	std::cout << "Duration: " << m << ":" << s << std::endl;


	//std::chrono::system_clock::time_point start = std::chrono::system_clock::now();

#if 0
	int tickPerBeat = ims->m_ims->header.nTickBeat;
	int basicTempo = ims->GetBasicTempo();
	int beatPerMin = basicTempo * tickPerBeat;
	int TPS = beatPerMin/60.;
	//int t = 1000/TPS;
#endif
	
	DWORD tid;
	CreateThread(NULL, 0, Update, (void*)ims, 0, &tid);

	while(m_playMode != SONG_END) {
#if 0
		std::chrono::duration<double> sec = std::chrono::system_clock::now() - start;
		long elapsed = sec.count();
		int h = elapsed/3600;
		int m = (elapsed%3600)/60;
		int s = (elapsed%3600)%60;
#endif

		int tickDelay = ims->Play();
		int basicTempo = ims->GetBasicTempo();
		int delay = MulDiv(1000 * 60, 
				tickDelay, basicTempo * header->header.nTickBeat);

		Sleep(delay);
	}

	return 0;
}
