all:
	g++ -c libserial.cpp
	g++ -c opl2board.cpp
	g++ -c adlib.cpp
	g++ -c outchip.cpp
	g++ -c setfreq.cpp
	g++ -c ims.cpp
	g++ -c bnk.cpp
	g++ -c convert.cpp
	g++ -static -o ims.exe \
			main.cpp libserial.o opl2board.o setfreq.o \
			outchip.o adlib.o ims.o bnk.o convert.o
	strip ./ims.exe
