#define STOP		0
#define PLAYING		1
#define STOPPED		2
#define SONG_END	3

#define REPEAT_NO		0	// 지금 곡 연주하고 중지
#define REPEAT_ALL		1	// 지금 곡 연주 끝나면 다음곡으로..
#define REPEAT_THIS		2	// 지금 곡 반복 연주

extern int m_playMode;
extern int m_repeatMode;

