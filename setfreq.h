#ifndef __SETFREQ_H_
#define __SETFREQ_H_

#include "opl2board.h"

int SetFreq(OPL2AudioBoard *opl, short voice, unsigned short note, short pitch, short keyOn);

#endif	//__SETFREQ_H_
